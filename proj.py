
from collections import deque
import numpy as np
import argparse
import imutils
import cv2

from sklearn.cross_validation import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn import datasets
 

## Funkcja przeksztakcebua zielonego BGR = HSV
#green = np.uint8([[[0,255,0 ]]])
#grenetohsv = cv2.cvtColor(green,cv2.COLOR_BGR2HSV)

print "pobieranie danych klasyfikatora MNIST... Prosze czekac :)"

## Pobieramy baze Mnist dla klasyfikatora
dataset = datasets.load_digits()

## Podzial zbioru danych na czesc treningowa i testowa 

(trainData, testData, trainLabels, testLabels) = train_test_split(dataset.data / 255.0, dataset.target.astype("int0"), test_size = 0)

## Tworzenie klasyfikatora 
model = KNeighborsClassifier(n_neighbors=1)


## Uczelnie klasyfikatora 
model.fit(trainData, trainLabels)

## Definiujemy zakres koloru w HSV
mingreen = (40, 100, 50)
maxgreen = (80, 255, 255)

# definiowane kolejki
pts = deque()


## Definicja obiektu z indeksem urzadzenia
cap = cv2.VideoCapture(1)


## Definicja pustego obrazu na ktorym bedzie rysowana maska znaku pokazywanego do kamery
character_mask = np.zeros((600, 480), dtype = "uint8")

## Definicja obiektu przechwujacego narysowany obiekt
character = None

while True:

	# Przechwytywanie klatek
	ret, frame = cap.read()

	# Odwrocenie lustrzanie obrazu z kamery

	frame = cv2.flip(frame,1)
	# Konwertujemy BGR TO HSV ## obraz oryginalny
	new_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	# Nalozenie maski ograniczajacej wykrycie tylko zielonego koloru
	mask = cv2.inRange(new_hsv, mingreen, maxgreen)

	## Uzycie funkcji erode i dilate do redukcji "szumow" na masce
	mask = cv2.erode(mask, None, iterations=2)
	mask = cv2.dilate(mask, None, iterations=2)

	## Znalezienie konturow wykrytego obiektu 
	contours= cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]
	
	## Definicja obiektu przechowujacego srodek wykrytego obiektu
	center = None
 
	## Definicja warunku - jesli znaleziono kontur
	if len(contours) > 0:

		## Znalezienie najwiekszego konturu
		c = max(contours, key=cv2.contourArea)

		## Funkcja cv2.minEnclosingCircle w celu znalezienia najmniejszego okregu do otoczenia znalezionego obiektu (konturow)
		(x, y) = cv2.minEnclosingCircle(c)
		
		## Funkcja cv2.moments do znalezienia srodka obiektu
		M = cv2.moments(c)

		 ## Wyznaczenie srodka okregu za pomoca obliczonych wczesniej momentow
		center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
		
		## Dodajemy do kolejki wspolrzedne srodna okregu	
		pts.appendleft(center)

		## Iteracja po kolejce
		for i in xrange(1, len(pts)):

			## rysujemy linie na podstawie punktow 
			cv2.line(frame, pts[i - 1], pts[i], (0, 0, 255), 5)

		if key == ord(" "):

			## Tworzymy czarny obraz 
			character_mask = np.zeros((600, 480), dtype = "uint8")


			## iterujemy ponownie po kolejce przrzucajac narysowany obraz na amaske
			for i in xrange(1, len(pts)):
				

					## rysowanie linii na masce
				cv2.line(character_mask, pts[i - 1], pts[i], (255, 255, 255), 5)

			## Usuwanie szumow 
			cv2.dilate(character_mask, None, iterations=3)

			## Wykrywanie zewnetrznych kontorow (na obrazie z maski)
			cnts, _ = cv2.findContours(character_mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
			
			## Znalezienie najwiekszego konturu
			c = max(cnts, key=cv2.contourArea)

			## Opisanie prostokatu na znalezionym konturze
			(x, y, w, h) = cv2.boundingRect(c)

			## Wycinamy namalowany znak z tla
			char = character_mask[y:y+h, x:x+w]

			## Robimy resize wycietego obrazka
			roi = cv2.resize(char, (8, 8))

			## Przeksztalcamy obraz na postac macierzy 
			roismall = roi.reshape((1, 8 * 8))
			
			## Przewidujemy prawdopodobniestwo do jakiego obrazku ze wzorca ten obraz jest podobny 
			prediction = model.predict(roismall)[0]

			## Konwersja z float do stringa
			character = str(prediction)
	

	key = cv2.waitKey(1) & 0xFF
	if key == ord("q"):
		break
	elif key == ord("r"):
		pts.clear()

 
 ## wyswietlamy rozpoznany znak 
 	if character != None:
		cv2.putText(frame, character, (frame.shape[1] - 200, frame.shape[0] - 20), cv2.FONT_HERSHEY_SIMPLEX, 2.0, (0, 0, 255), 3)

 	cv2.imshow("Frame", frame)


cv2.destroyAllWindows()
